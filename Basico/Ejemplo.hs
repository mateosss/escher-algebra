module Basico.Ejemplo where
import Dibujo
import Interp

data Bas = Triangulo1 | Triangulo2 | TrianguloDown | Rectangulo | FShape
  deriving Eq
instance Show Bas where
  show Triangulo1  = "Δ"
  show Triangulo2 = "◺"
  show TrianguloDown = "∇"
  show Rectangulo = "▫"
  show FShape = "F"

ejemplo :: Dibujo Bas
ejemplo = Encimar (Rot45 (Basica Triangulo1)) (Basica Triangulo1)

interpBas :: Output Bas
interpBas Triangulo1 = trian1
interpBas Triangulo2 = trian2
interpBas TrianguloDown = trianD
interpBas Rectangulo = rectan
interpBas FShape = fShape

-- desc example
toChineseString :: Bas -> String
toChineseString Triangulo1  = "三角形1"
toChineseString Triangulo2 = "三角形2"
toChineseString TrianguloDown = "倒三角形"
toChineseString Rectangulo = "矩形"
toChineseString FShape = "f的形式"

desc_chinese = desc toChineseString ejemplo
desc_example = desc show ejemplo

-- every example
every_ejemplo = every ejemplo
every_ejemplo1 = every limpia_a
every_ejemplo2 = every anyDib_dib

-- contar example
contar_a = ciclar (Basica Triangulo1)
contar_ejemplo = contar contar_a
contar_ejemplo1 = contar limpia_a
contar_ejemplo2 = contar anyDib_dib

-- limpia example
limpia_a = cuarteto (Encimar (Basica Triangulo1) (Basica Triangulo2)) (Basica TrianguloDown) (Rot45 (Basica Rectangulo)) (Basica Triangulo2)
limpia_f Triangulo2 = True
limpia_f _ = False
limpia_ejemplo = limpia limpia_f FShape limpia_a

-- anyDib example
anyDib_dib = Juntar 1 1 (Basica Rectangulo) (ciclar (Encimar (Basica FShape) (Basica Triangulo2)))
anyDib_true = anyDib (\b -> b == Rectangulo) anyDib_dib -- should be true
anyDib_false = anyDib (\b -> b == Triangulo1) anyDib_dib -- should be false

-- allDib example
allDib_dib = Juntar 1 1 (Basica Rectangulo) (ciclar (Encimar (Basica Rectangulo) (Basica Rectangulo)))
allDib_true = allDib (\b -> b == Rectangulo) allDib_dib -- should be true
allDib_false = allDib (\b -> b == Triangulo1) allDib_dib -- should be false

-- Checks examples
checks_rot360 d = Rotar (Rotar (Rotar (Rotar d)))
checks_flip2 d = Espejar (Espejar d)
checks_both = Juntar 1 1 (checks_rot360 (Espejar (checks_flip2 (Basica Triangulo1)))) (checks_rot360 (Basica Triangulo1))

checks_R1_error = check esRot360 "Error: tiene rotaciones 360" checks_both
checks_R2_pass = check esRot360 "Error: tiene rotaciones 360" (checks_flip2 (Basica Triangulo1))
checks_R3_pass = check esRot360 "Error: tiene rotaciones 360" (noRot360 checks_both)
checks_noRot = noRot360 checks_both

checks_F1_error = check esFlip2 "Error: tiene doble espejados" checks_both
checks_F2_pass = check esFlip2 "Error: tiene doble espejados" (checks_rot360 (Basica Triangulo1))
checks_F3_pass = check esFlip2 "Error: tiene doble espejados" (noFlip2 checks_both)
checks_noFlip = noFlip2 checks_both

-- todoBien example
todoBien_PP = todoBien allDib_dib -- both pass
todoBien_PE = todoBien (checks_flip2 (Basica Triangulo1)) -- flip2 error
todoBien_EP = todoBien (checks_rot360 (Basica Triangulo1)) -- rot360 error
todoBien_EE = todoBien checks_both -- both error

-- cambiar example
cambiar_ex = Encimar (Apilar 1 1 (Basica Triangulo1) (Basica Triangulo2)) (Basica TrianguloDown)
cambiar_f Triangulo1 = Rotar $ Rotar $ Rotar $ Basica Triangulo2
cambiar_f Triangulo2 = Basica Triangulo1
cambiar_f _ = Basica FShape
cambiar_example = cambiar cambiar_f cambiar_ex

-- mapDib example
mapDib_f Triangulo1 = Triangulo2
mapDib_f Triangulo2 = Triangulo1
mapDib_f _ = FShape
mapDib_example = mapDib mapDib_f cambiar_ex
