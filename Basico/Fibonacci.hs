module Basico.Fibonacci where

import Dibujo
import Interp
import Graphics.Gloss

data Fibonacci = Diagonal | Rectangulo | FShape | Curva

u = 1000000 -- rounding unit
golden_ratio = truncate $ ((1 + sqrt 5) / 2) * (fromIntegral u) -- scaled

interpFibonacci :: Picture -> Output Fibonacci
interpFibonacci _ Diagonal = trian2
interpFibonacci _ Rectangulo = rectan
interpFibonacci _ FShape = fShape
interpFibonacci _ Curva = spiral

fibonacci :: Int -> Dibujo Fibonacci -> Dibujo Fibonacci
fibonacci 0 b = b
fibonacci n b = Juntar u (golden_ratio - u) b (r270 $ fibonacci (n - 1) b)

bloque = Encimar (Basica Rectangulo) (Rotar $ Espejar $ Rotar $ Rotar $ Basica Curva)
dibujo = fibonacci 64 bloque
