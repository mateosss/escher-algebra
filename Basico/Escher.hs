module Basico.Escher where

import Dibujo
import Interp
import Graphics.Gloss

data Escher = F | Triangulo | Vacia | Img | Cuadrado | Curva


interpEscher :: Output Escher
interpEscher F = fShape
interpEscher Vacia = simple blank
interpEscher Triangulo = trian2
interpEscher Img = trian2
interpEscher Cuadrado = rectan

interpEscher' :: Picture -> Output Escher
interpEscher' _ F = fShape
interpEscher' _ Vacia = simple blank
interpEscher' _ Triangulo = trian2
interpEscher' _ Cuadrado = rectan
interpEscher' _ Curva = curvita
interpEscher' p Img = encimar trian2 (transf f 1 (123, 123))
  where f _ _ = p
-- interpEscher' p Img = (transf f 1 (123, 123))

vacia = pureDib Vacia
f = pureDib F
t = pureDib Triangulo

-- el dibujo u
dibujo_u :: Dibujo Escher -> Dibujo Escher
dibujo_u p = encimar4 p2
    where p2 = Espejar $ Rot45 p

-- el dibujo t
dibujo_t :: Dibujo Escher -> Dibujo Escher
dibujo_t p = Encimar p $ Encimar p2 p3
    -- where p2 = Rot45 p
    where p2  = Espejar $ Rot45 p
          p3 = r270 p2

-- esquina con nivel de detalle en base a la figura p
esquina :: Int -> Dibujo Escher -> Dibujo Escher
esquina 0 _ = vacia
esquina n p = cuarteto esquina' lado' (Rotar lado') u'
    where esquina' = esquina (n-1) p
          lado' = lado (n-1) p
          u' = dibujo_u p

-- lado con nivel de detalle
lado :: Int -> Dibujo Escher -> Dibujo Escher
lado 0 _ = vacia
lado n p = cuarteto lado' lado' (Rotar t') t'
    where lado' = lado (n-1) p
          t' = dibujo_t p

-- ordena los nueve elementos en una grilla de 3x3
noneto p q r s t u v w x = row (col p q r) (col s t u) (col v w x)
    where row a b c = Apilar 1 2 a (Apilar 1 1 b c)
          col a b c = Juntar 1 2 a (Juntar 1 1 b c)

-- el dibujo de Escher
escher :: Int -> Escher -> Dibujo Escher
escher n e = noneto esq         lad          (r270 esq)
                    (Rotar lad) (dibujo_u p) (r270 lad)
                    (Rotar esq) (r180 lad)   (r180 esq)
    where esq = esquina n p
          lad = lado n p
          p = pureDib e

-- figuras para comparar las construcciones
v = ciclar $ Rotar $ dibujo_t t

lado1 = cuarteto vacia vacia (Rotar t') t'
    where t' = dibujo_t t

lado2 = cuarteto lado1 lado1 (Rotar t') t'
    where t' = dibujo_t t

esquina1 = cuarteto vacia vacia vacia u'
    where u' = dibujo_u t

esquina2 = cuarteto esquina1 lado1 (Rotar lado1) u'
    where u' = dibujo_u t

dibujo = escher 3 Img
