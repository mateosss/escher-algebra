module Interp where
import Graphics.Gloss
import Graphics.Gloss.Data.Vector
import Graphics.Gloss.Geometry.Angle
import qualified Graphics.Gloss.Data.Point.Arithmetic as V

import Dibujo
type FloatingPic = Vector -> Vector -> Vector -> Picture
type Output a = a -> FloatingPic

-- el vector nulo
zero :: Vector
zero = (0,0)

half :: Vector -> Vector
half = (0.5 V.*)

-- comprender esta función es un buen ejericio.
hlines :: Vector -> Float -> Float -> [Picture]
hlines v@(x,y) mag sep = map (hline . (*sep)) [0..]
  where hline h = line [(x,y+h),(x+mag,y+h)]

-- Una grilla de n líneas, comenzando en v con una separación de sep y
-- una longitud de l (usamos composición para no aplicar este
-- argumento)
grid :: Int -> Vector -> Float -> Float -> Picture
grid n v@(x, y) sep l = pictures [ls,translate (x-y) (l+x+y) (rotate 90 ls)]
  where ls = pictures $ take (n+1) $ hlines v l sep

-- figuras adaptables comunes
trian1 :: FloatingPic
trian1 a b c = line $ map (a V.+) [zero, half b V.+ c , b , zero]

trian2 :: FloatingPic
trian2 a b c = line $ map (a V.+) [zero, c, b,zero]

trianD :: FloatingPic
trianD a b c = line $ map (a V.+) [c, half b , b V.+ c , c]

rectan :: FloatingPic
rectan a b c = line [a, a V.+ b, a V.+ b V.+ c, a V.+ c,a]

simple :: Picture -> FloatingPic
simple p _ _ _ = p

fShape :: FloatingPic
fShape a b c = line . map (a V.+) $ [ zero,uX, p13, p33, p33 V.+ uY , p13 V.+ uY
                 , uX V.+ 4 V.* uY ,uX V.+ 5 V.* uY, x4 V.+ y5
                 , x4 V.+ 6 V.* uY, 6 V.* uY, zero]
  where p33 = 3 V.* (uX V.+ uY)
        p13 = uX V.+ 3 V.* uY
        x4 = 4 V.* uX
        y5 = 5 V.* uY
        uX = (1/6) V.* b
        uY = (1/6) V.* c

(x,y) .+ (x',y') = (x+x',y+y')
s .* (x,y) = (s*x,s*y)
(x,y) .- (x',y') = (x-x',y-y')
negar (x,y) = (-x,-y)

curvita :: FloatingPic
curvita a b c = line $ bezier3 a (a .+ b .+((1/3) .* c)) (a .+ b .+ c) 10

spiral :: FloatingPic
spiral a b c = line $ bezier4 a (a .+ ((1/slope) .* b)) (a .+ b .+ ((1/slope) .* c)) (a .+ b .+ c) 32
  where slope = 64

bezier3 :: Vector -> Vector -> Vector -> Int -> [Vector]
bezier3 p0 p1 p2 n = [ p1 .+ (((1-t)^2) .* (p0 .+ (negar p1))) .+ ((t^2) .* (p2 .+ (negar p1))) | t <- ts]
 where ts = 0:map (divF n) [1..n]
       divF :: Int -> Int -> Float
       divF j i = toEnum i / toEnum j

bezier4 :: Vector -> Vector -> Vector -> Vector -> Int -> [Vector]
bezier4 p0 p1 p2 p3 n = [(((1 - t)^3) .* p0) .+ ((3 * ((1 - t) ^ 2) * t) .* p1) .+ ((3 * (1 - t) * (t ^ 2)) .* p2) .+ (((t ^ 3)) .* p3) | t <- ts]
 where ts = 0:map (divF n) [1..n]
       divF :: Int -> Int -> Float
       divF j i = toEnum i / toEnum j

-- Dada una función que produce una figura a partir de un a y un vector
-- producimos una figura flotante aplicando las transformaciones
-- necesarias. Útil si queremos usar figuras que vienen de archivos bmp.
transf :: (a -> Vector -> Picture) -> a -> Vector -> FloatingPic
transf f d (xs,ys) a b c  = uncurry translate (((153 - 123) / 153 - 0.07) V.* (V.negate b)) .
                            translate (fst a') (snd a') .
                            scale (magV b/xs) (-(magV c/ys)) .
                            rotate ang $ f d (xs,ys)
  where ang = radToDeg $ argV b
        a' = a V.+ half (b V.+ c)


-- interp :: (a -> FloatingPic) -> Dibujo a -> FloatingPic
interp :: Output a -> Output (Dibujo a)
interp f p = sem f rotar espejar rot45 apilar juntar encimar p

-- Semantica de predicado
rotar :: FloatingPic -> FloatingPic
rotar p a b c = p (a V.+ b) c (V.negate b)

espejar :: FloatingPic -> FloatingPic
espejar p a b c = p (a V.+ b) (V.negate b) c

rot45 :: FloatingPic -> FloatingPic
rot45 p a b c = p a' b' c'
    where a' = a V.+ half (b V.+ c)
          b' = half (b V.+ c)
          c' = half (c V.- b)

apilar :: Int -> Int -> FloatingPic -> FloatingPic -> FloatingPic
apilar m n p q a b c = pictures [p (a V.+ c') b (r' V.* c), q a b c']
    where c' = r V.* c
          r = n `_div` (m + n)
          r' = m `_div` (m + n)
          _div a b = (fromIntegral a) / (fromIntegral b)

juntar :: Int -> Int -> FloatingPic -> FloatingPic -> FloatingPic
juntar m n p q a b c = pictures [p a b' c, q (a V.+ b') (r' V.* b) c]
  where b' = r V.* b
        r = m `_div` (m + n)
        r' = n `_div` (m + n)
        _div a b = (fromIntegral a) / (fromIntegral b)

encimar :: FloatingPic -> FloatingPic -> FloatingPic
encimar p q a b c = pictures [p a b c, q a b c]
