module Dibujo where
import Data.Either (lefts, rights)

data Dibujo a = Basica a | Rotar (Dibujo a) | Espejar (Dibujo a)
              | Rot45 (Dibujo a)
              | Apilar Int Int (Dibujo a) (Dibujo a)
              | Juntar Int Int (Dibujo a) (Dibujo a)
              | Encimar (Dibujo a) (Dibujo a)
              deriving (Show, Eq)
instance Functor Dibujo where
    fmap f p = p >>= (\a -> pure $ f a)
instance Applicative Dibujo where
    pure = Basica
    (<*>) = undefined
instance Monad Dibujo where
    p >>= f = sem f Rotar Espejar Rot45 Apilar Juntar Encimar p

sem :: (a -> b) -> (b -> b) -> (b -> b) -> (b -> b) ->
       (Int -> Int -> b -> b -> b) ->
       (Int -> Int -> b -> b -> b) ->
       (b -> b -> b) ->
       Dibujo a -> b
sem basica rotar espejar rot45 apilar juntar encimar p =
    case p of (Basica b) -> basica b
              (Rotar p) -> rotar (sem' p)
              (Espejar p) -> espejar (sem' p)
              (Rot45 p) -> rot45 (sem' p)
              (Apilar n m p q) -> apilar n m (sem' p) (sem' q)
              (Juntar n m p q) -> juntar n m (sem' p) (sem' q)
              (Encimar p q) -> encimar (sem' p) (sem' q)
    where sem' = sem basica rotar espejar rot45 apilar juntar encimar


-- Composición n-veces de una función con sí misma.
comp :: (a -> a) -> Int -> a -> a
comp f 0 = id
comp f n = f . (comp f (n - 1))

-- Rotaciones de múltiplos de 90.
r180 :: Dibujo a -> Dibujo a
r180 p = Rotar (Rotar p)

r270 :: Dibujo a -> Dibujo a
r270 p = Rotar (r180 p)

-- Pone una figura sobre la otra, ambas ocupan el mismo espacio
(.-.) :: Dibujo a -> Dibujo a -> Dibujo a
(.-.) p q = Apilar 1 1 p q

-- Pone una figura al lado de la otra, ambas ocupan el mismo espacio
(///) :: Dibujo a -> Dibujo a -> Dibujo a
(///) p q = Juntar 1 1 p q

-- Superpone una figura con otra
(^^^) :: Dibujo a -> Dibujo a -> Dibujo a
(^^^) p q = Encimar p q

-- Dada cuatro figuras las repite en cuatro cuadrantes
cuarteto :: Dibujo a -> Dibujo a -> Dibujo a -> Dibujo a -> Dibujo a
cuarteto p q r s = (p /// q) .-. (r /// s)

-- Una figura repetida con las cuatro rotaciones, superimpuestas.
encimar4 :: Dibujo a -> Dibujo a
encimar4 p = p ^^^ Rotar p ^^^ r180 p ^^^ r270 p

-- Cuadrado con la misma figura rotada 4 veces
ciclar :: Dibujo a -> Dibujo a
ciclar p = cuarteto p (r270 p) (Rotar p) (r180 p)

-- Ver un a como una figura
pureDib :: a -> Dibujo a
pureDib = pure

-- Mapea las bases de tipo a de un Dibujo a nuevas bases de tipo b
mapDib :: (a -> b) -> Dibujo a -> Dibujo b
mapDib = fmap

-- Cambia cada base por un nuevo dibujo
cambiar :: (a -> Dibujo b) -> Dibujo a -> Dibujo b
cambiar f p = p >>= f


-- Predicates

type Pred a = a -> Bool

-- Dado un predicado sobre básicas, cambiar todas las que satisfacen
-- el predicado por la figura básica indicada por el segundo argumento.
limpia :: Pred a -> a -> Dibujo a -> Dibujo a
limpia f empty p = mapDib emptyIfPred p
    where emptyIfPred b = if f b then empty else b

-- Alguna básica satisface el predicado
anyDib :: Pred a -> Dibujo a -> Bool
anyDib f p = sem f id id id (\_ _ -> (||)) (\_ _ -> (||)) (||) p

-- Todas las básicas satisfacen el predicado
allDib :: Pred a -> Dibujo a -> Bool
allDib f p = sem f id id id (\_ _ -> (&&)) (\_ _ -> (&&)) (&&) p


-- Describe el dibujo usando una función de traducción
desc :: (a -> String) -> Dibujo a -> String
desc t p = sem t (chain "rot") (chain "esp") (chain "r45")
           (wfork "api") (wfork "jun") (fork "enc") p
    where fork s p q = s ++ "(" ++ p ++ ", " ++ q ++ ")"
          wfork s _ _ p q = fork s p q
          chain s p = s ++ "(" ++ p ++ ")"

-- Devuelve una lista con todas las básicas del dibujo
every :: Dibujo a -> [a]
every p = sem (\a -> [a]) id id id (\_ _ -> (++)) (\_ _ -> (++)) (++) p

-- Cuenta la cantidad de veces que aparecen las básicas en una figura.
contar :: Eq a => Dibujo a -> [(a, Int)]
contar p = sem add id id id (\_ _ -> (.)) (\_ _ -> (.)) (.) p []
    where add b [] = [(b, 1)]
          add b (bc:bcs) | (fst bc) == b = (b, (snd bc) + 1):bcs
                         | otherwise = bc:(add b bcs)


-- Checks

-- Hay 4 rotaciones seguidas (empezando en el tope)
esRot360 :: Pred (Dibujo a)
esRot360 (Rotar (Rotar (Rotar (Rotar _)))) = True
esRot360 (Basica _) = False
esRot360 (Rotar p) = esRot360 p
esRot360 (Espejar p) = esRot360 p
esRot360 (Rot45 p) = esRot360 p
esRot360 (Apilar _ _ p q) = esRot360 p || esRot360 q
esRot360 (Juntar _ _ p q) = esRot360 p || esRot360 q
esRot360 (Encimar p q) = esRot360 p || esRot360 q

-- Hay 2 espejados seguidos (empezando en el tope)
esFlip2 :: Pred (Dibujo a)
esFlip2 (Espejar (Espejar p)) = True
esFlip2 (Basica _) = False
esFlip2 (Rotar p) = esFlip2 p
esFlip2 (Espejar p) = esFlip2 p
esFlip2 (Rot45 p) = esFlip2 p
esFlip2 (Apilar _ _ p q) = esFlip2 p || esFlip2 q
esFlip2 (Juntar _ _ p q) = esFlip2 p || esFlip2 q
esFlip2 (Encimar p q) = esFlip2 p || esFlip2 q

-- Si el dibujo no cumple el predicado, devuelve el string de error
check :: Pred (Dibujo a) -> String -> Dibujo a -> Either String (Dibujo a)
check p err d = if not (p d) then Right d else Left err

-- Aplica todos los chequeos y acumula todos los errores,
-- sólo devuelve la figura si no hubo ningún error.
todoBien :: Dibujo a -> Either [String] (Dibujo a)
todoBien p = if null errs then Right (head dibs) else Left errs
    where rc = check esRot360 "tiene rotaciones 360" p
          fc = check esFlip2 "tiene espejados dobles" p
          errs = lefts [rc, fc]
          dibs = rights [rc, fc]

-- Simplifica una figura con cuatro rotaciones seguidas
noRot360 :: Dibujo a -> Dibujo a
noRot360 (Rotar (Rotar (Rotar (Rotar p)))) = noRot360 p
noRot360 (Rotar p) = Rotar (noRot360 p)
noRot360 (Espejar p) = Espejar (noRot360 p)
noRot360 (Rot45 p) = Rot45 (noRot360 p)
noRot360 (Apilar n m p q) = Apilar n m (noRot360 p) (noRot360 q)
noRot360 (Juntar n m p q) = Juntar n m (noRot360 p) (noRot360 q)
noRot360 (Encimar p q) = Encimar (noRot360 p) (noRot360 q)
noRot360 p@(Basica _) = p

-- Simplifica una figura con dos espejados seguidos
noFlip2  :: Dibujo a -> Dibujo a
noFlip2 (Espejar (Espejar p)) = noFlip2 p
noFlip2 (Rotar p) = Rotar (noFlip2 p)
noFlip2 (Espejar p) = Espejar (noFlip2 p)
noFlip2 (Rot45 p) = Rot45 (noFlip2 p)
noFlip2 (Apilar n m p q) = Apilar n m (noFlip2 p) (noFlip2 q)
noFlip2 (Juntar n m p q) = Juntar n m (noFlip2 p) (noFlip2 q)
noFlip2 (Encimar p q) = Encimar (noFlip2 p) (noFlip2 q)
noFlip2 p@(Basica _) = p
