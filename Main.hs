module Main where
import Graphics.Gloss
import Graphics.Gloss.Interface.IO.Display
import Graphics.UI.GLUT.Begin
import Dibujo
import Interp
import qualified Basico.Escher as E
import qualified Basico.Fibonacci as F

data Conf a = Conf {
    basic :: Picture -> Output a
  , fig  :: Dibujo a
  , width :: Float
  , height :: Float
  }

-- Escher config
escher_ej x y = Conf {
                basic = E.interpEscher'
              , fig = E.dibujo
              , width = x
              , height = y
              }
fish_path = "img/fish.bmp"
escher_win_size = (1000, 1000)

-- Fibonacci config
fib_ej x y = Conf {
                basic = F.interpFibonacci
              , fig = F.dibujo
              , width = x
              , height = y
              }
fib_win_size = (1.618 * 384, 384)


toBottom :: Picture -> Picture
toBottom = uncurry translate $ tMap _half win_size
  where tMap f (a, b) = (f a, f b)
        _half n = (-0.5) * n

-- Da una grilla cuadrada de tamaño l, con n cols/rows
_grid :: Float -> Int -> Picture
_grid l n = grid n (0, 0) sep l
  where sep = l / fromIntegral n
-- grid 10 (0,0) 100 10

-- Dada una computación que construye una configuración, mostramos por
-- pantalla la figura de la misma de acuerdo a la interpretación para
-- las figuras básicas. Permitimos una computación para poder leer
-- archivos, tomar argumentos, etc.

initial :: IO (Conf E.Escher) -> IO ()
initial cf = cf >>= \cfg ->
                  let x  = width cfg
                      y  = height cfg
                  in do
                    fish <- loadBMP fish_path
                    display win white . toBottom $ interp ((basic cfg) fish) (fig cfg) (0,0) (x,0) (0,y)
  where withGrid p = pictures [p, color grey $ _grid (fst win_size) 100]
        grey = makeColorI 120 120 120 120


win_size :: (Float, Float)
win_size = escher_win_size
ej = escher_ej

win = InWindow "Escher" (toIntT win_size) (0, 0)
    where toIntT (a, b) = (floor a, floor b)

main = initial $ return (uncurry ej win_size)
