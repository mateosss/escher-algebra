# Un lenguaje vale más que mil dibujos

## Introducción

En este laboratorio tuvimos la tarea de implementar un pequeño lenguaje de
dominio específico. El objetivo es dar una abstracción desde la cual poder crear
dibujos, pensando en la relación que tienen las partes que lo componen, y
centrándonos en las descripciones algebraicas que definen estas relaciones.

Para definir el álgebra (o el DSL) utilizamos la sintaxis funcional y el sistema
de tipos de haskell que nos facilitó la definición de nuestras abstracciones.
Por otro lado, para la representación geométrica en la pantalla utilizamos la
librería `gloss` junto al tipo `FloatingPic` para representar transformaciones
en la imagen.


## Functores, Aplicativos y Mónadas

Se habló mucho de mónadas en el transcurso del proyecto, y en muchas páginas nos
topábamos con el concepto. Al no entenderlo, muchas de estas páginas nos eran
inútiles. No ayudó el hecho de que gran parte de estas páginas recomendaban un
doctorado en computación para entender el concepto. Así que decidimos encontrar
algún tutorial simple que explique con cajitas el concepto *(a pesar de que
específicamente nos dijeron sobre no leer explicaciones de este tipo)*.

[Éste](http://adit.io/posts/2013-04-17-functors,_applicatives,_and_monads_in_pictures.html)
fue el primer recurso que nos introdujo al concepto. Notamos como `mapDib` era
similar al comportamiento de un functor y `cambiar` al de una mónada. Al ver las
similitudes decidimos que `mapDib` y `cambiar` no deberían ser más que sinónimos
de las funciones `fmap` y `>>=`. Algunas cosas que quedaron extrañas por hacerlo
de esta forma, es que `mapDib` lo definíamos en función de `cambiar`, entonces
el `fmap`, quedó definido en función de `>>=`, lo cual es un poco contra
intuitivo *(ya que `Monad` es algo superior a `Functor`)*. A su vez, para
instanciar `Monad`, haskell nos obliga a instanciar `Applicative`, el cuál no
tiene una aplicación directa con nuestro `Dibujo`, es por esto que la dejamos
`undefined` *(salvo por `pure` que era como `pureDib`)*.

Nos volvimos a topar con el concepto de mónada al cargar el archivo bmp, y en
algunos otros momentos. Entendemos que se usa como una forma de separar el mundo
*"puro"* por así decirlo, con el *"impuro"* que es el de la computadora, el que
tiene que manejar problemas. Como lo es querer definir una función `dividir` que
divide dos `Float` pero al saber que esto puede dar error si dividimos por 0,
entonces no podemos asegurar que vamos a devolver un `Float` y devolvemos `Maybe
Float`. Suponemos entonces que la mónada `IO` se encarga de aspectos similares
relacionados al IO de la computadora. Que pasa si hay un error de lectura? un
error de print a pantalla? y otro sinfín de problemas.

Exploramos también un poco de la notación `do` y nos resultó interesante como,
al usarla, de repente haskell se transforma en un lenguaje imperativo, aunque en
realidad esté usando conceptos funcionales por detrás del `do`.

Nos falta bastante para interiorizar bien el concepto, y quizás se pueda
entender mejor aplicándolo a un proyecto, pero esto fue lo que conseguimos
comprender, y fue interesante.


## Sistema de transformaciones

Creemos que el sistema de transformaciones de 3 vectores (`a`, `b` y `c`)
adoptado en el paper y en el proyecto fueron un poco contraproducentes,
considerando que gloss ya aporta las funciones `translate`, `rotate` y `scale`,
y en motores de juegos o programas gráficos en general, esta es la forma en la
que se suelen manipular las transformaciones de un objeto 2d.

Si bien fue un buen recordatorio de análisis 2, creemos que nos impidió poder
hacer cosas más elaboradas gráficamente. Sobre todo al intentar arreglar la
implementación de `transf`.


## Función `sem :: ... -> Dibujo a -> b`

Comenzamos el proyecto sin tener ni la menor idea de en donde encajaría `sem`, y
de hecho lo terminamos sin `sem`. Sólo con pattern matching y, una vez definido
`every`, funciones que ya existían en listas (como `all` o `any`, o hacer
`foldl` para implementar `contar`).

Al comenzar a implementarla nos dimos cuenta de lo interesante de la
abstracción, como atrapaba ciertos patrones muy bien, y como el tipo `b` en la
definición podía ser cualquier cosa que quisiéramos.

Al final creemos haberlo entendido y aplicado bastante bien, aunque funciones
como `contar`, nunca se nos hubieran ocurrido como implementar sin el indicio de
que el tipo `b` debería ser `[(a, Int)] -> [(a, Int)]`.

Por último, se nos recomendó escribir `sem` con pattern matching, por ser una
práctica común de haskell, pero preferimos dejarla con `case of` por la
legibilidad que ofrece.


## Escher

> Al fín!

Luego de tanto *esfuerzo*, llegó la parte más divertida: Dibujar.

La consigna nos pedía que reconstruyamos el gráfico Escher. Y cuando decimos
gráfico, nos referimos a su famoso trabajo de grabado xilográfico **Square
Limit** *(1964)*

![SquereLimit](https://media.nga.gov/iiif/640/public/objects/1/3/5/6/0/4/135604-primary-0-nativeres.ptif/full/!440,400/0/default.jpg)

Lo que en principio parecía una tarea monumental, poco a poco se fue
esclareciendo al armar las partes por separado y luego componerlas.

Para una primera aproximación remplazamos la figura del pescado de escher por un
triangulo rectángulo. Usamos esta misma figura definimos la estructura
`dibujo_t` y `dibujo_u`, nuestras bases durante todo el dibujo.

Si vemos a Escher como una grilla de tres filas y tres columnas, podemos
apreciar que hay un centro (que se mantiene en todas las profundidades), un lado
y una esquina. Cada una de estas últimas repetidas en las cuatro direcciones.

Para entender e implementar las funciones recursivas que componen estos lados y
esquinas, nos apoyamos fuertemente en el artículo de Peter Henderson
[*Functional
Geometry*](https://cs.famaf.unc.edu.ar/~mpagano/henderson-funcgeo2.pdf).

Luego de componer todos estos elementos (usando nuestra aclamada función
`noneto`) y ajustar debidamente el Main (jugamos un poco con el tamaño de la
ventana y preferimos desplazar el origen del dibujo a su vértice inferior
izquierdo) el resultado fue el siguiente:

![Square Limit con triángulos, profundidad 3](img/squarelimit3_triangles.png)


### Va tomando forma

Inspirados y felices por nuestro pequeño logro, el siguiente paso obvio era
cambiar nuestra simplificación de triángulos, por una base más similar a la
original de Escher. Con ayuda de la cátedra, tuvimos acceso a una imagen en mapa
de bits, con la representación de tan especial pez.

Valiéndonos de la librería `Gloss` pudimos cargar dicha imagen en forma de
`Picture`. O Mejor dicho, en forma de `IO Picture`. Esto último trajo algunos
problemas, consecuencia de nuestro poco manejo de mónadas; pero nada de lo que
la sentencia `do` no pueda salvarnos.

Para poder unir nuestro flamante pez con la lógica de Escher, agregamos una
básica llamada `Img` que adicionalmente toma una `Picture`. También definimos
una nueva función para interpretar básicas, que contemple los nuevos tipos en
los constructores. En el caso de `Img` esta interpretación toma una imagen (en
particular del pez) y devuelve una interpretación de ella, alineada
especialmente con nuestro antiguo triangulo rectángulo

![fishVsTrian](img/fish_vs_trian.png)

Luego de pelear lo necesario con la función encargada de las transformaciones
que afectaban a estas nuevas imágenes ingresadas por archivos, el resultado
final al que arribamos fue el siguiente:

![Square Limit con peces, profundidad 3](img/squarelimit3_fishes.png)

Lamentablemente, no todas son buenas noticias. La realidad es que esa misma
función `transf` no tiene una implementación que nos permita tener el efecto
deseado, con cualquier imagen que tengamos. Nos queda pendiente este reto, para
retomarlo luego de la entrega.

Durante el proceso de calibración que sufrió `transf`, se arrojaron algunos
resultados visualmente interesantes. No queríamos perdernos la oportunidad de
compartir, uno de nuestros favoritos:

![Variante de Square Limit con peces, profundidad 3](img/squarelimit_variant3_fishes.png)


### Fibonacci

No tuvimos demasiado tiempo para implementar más cosas, pero algo que pensamos
que no podía faltar luego de ver la naturaleza recursiva de los dibujos, era
reproducir la imagen de la espiral de fibonacci. Para esta utilizamos la función
`bezier4` que es una variante de la `bezier` (ahora llamada `bezier3`), que
implementa la fórmula para 4 puntos.

Para simular los Integers que el constructor `Juntar` espera, utilizamos el
número áureo ($\varphi=\frac{1 + \sqrt{5}}{2}$) multiplicado por un entero de
*redondeo* lo suficientemente grande.

![Fibonacci](img/fibonacci.png)


### Conclusiones

El animo general del grupo, es positivo.

Este laboratorio nos sirvió para poner en practica algunos conceptos teóricos
vistos en clase (incluso de otras materias y otros años). Pudimos admirar y
amigarnos un poco más con el paradigma funcional/declarativo que nos ofrece
Haskell y el uso de librerías externas. Esta instancia también nos sirvió para
aceitar cosas como el trabajo en equipo con varios integrantes y el uso de
sistemas de control de versiones.

Nos hubiera gustado explorar un poco más otras ideas. Como el uso de registros
para darle colores a básicas, o valernos de `Gloss` para extender nuestro
lenguaje a dibujos y animaciones. Eso sumado a darle una mejor implementación a
alguna función que nos dios problemas, son saldos pendientes que nos quedan; y
los cuales esperamos, retomar en algún futuro cercano.
