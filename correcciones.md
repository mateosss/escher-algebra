## Corrección		
	Tag o commit corregido:	lab-1
		
### Entrega		100.00%
	Tag correcto	100.00%
	En tiempo	100.00%
### Funcionalidad		100.00%
	No hay errores de tipo	100.00%
	Se puede compilar y ejecuta correctamente	100.00%
	Esquemas para manipulación	100.00%
	Consultas (definibles por esquemas, `contar` sólo suma)	100.00%
	Chequeos y corrección de rot360 y flipFlip	100.00%
	Interpretación geométrica	100.00%
	Escher	100.00%
		
### Modularización y diseño		100.00%
	Dibujo es paramétrico	100.00%
	Alto orden para consultas	100.00%
	Identificación de situaciones recurrentes (parentizado en descr, por ejemplo)	100.00%
	Correcta elección de tipo para Escher (con justificación)	100.00%
### Calidad de código		97.50%
	Buenas prácticas funcionales	100.00%
	Líneas de más de 80 caracteres	75.00%
	estilo	100.00%
		
		
		
### Uso de git		100.00%
	Commits frecuentes	100.00%
	Nombres de commits significativos	100.00%
	Commits de todes les integrantes	100.00%
### Opcionales		
		
	Puntos estrella	100.00%
		
# Nota Final		10
		
		
# Comentarios		
		
- Algunas líneas se pasan por mucho de los 80 caracteres		
- Excelente uso de Haskell en general. Muy buen uso de modularización, diseño y funciones auxiliares		
